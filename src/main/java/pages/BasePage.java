package pages;

import annotations.Name;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import steps.ButtonSteps;
import steps.CheckSteps;
import steps.FieldSteps;
import utils.Reflection;

import java.lang.reflect.Field;
import java.util.Arrays;

import static com.codeborne.selenide.Selenide.$;

public abstract class BasePage<T> {

//----------------------------------------------[Steps]---------------------------------------------------------------
  @Step("Кликнуть на кнопку '{buttonName}'")
  public void clickInButton(String buttonName){
    SelenideElement button = getElementByName(buttonName)
            .shouldBe(Condition.visible
                    .because("Кнопка " + buttonName + " не найдена на странице"));
    button.click();
  }

  @Step("На странице отображается элемент '{elementName}'")
  public void elementIsVisible(String elementName){
    getElementByName(elementName)
            .shouldBe(Condition.visible.because("Элемент " + elementName + " не найден на странице"));
  }

  @Step("Ввести в поле {fieldName} значение '{value}'")
  public void enterValueInField(String fieldName, String value){
    SelenideElement field = getFieldByName(fieldName)
            .shouldBe(Condition.visible.because("Поле не найдено"));
    field.setValue(value);
  }

  @Step

  //-----------------------------------------------[Others]------------------------------------------------------------
  public SelenideElement getElementByName(String elementName) {
    Field field = null;
    Class clazz = this.getClass();
    // Проходим по цепочке наследования, если в текущем классе данное поле не описано
    while (clazz != BasePage.class) {
      // Забираем все поля класса и ищем то, у которого аннотация Name равна искомому имени элемента
      field = Arrays.stream(clazz
                      .getDeclaredFields()).filter(f -> f.getDeclaredAnnotation(Name.class) != null)
              .filter(f -> f.getDeclaredAnnotation(Name.class).value()
                      .equals(elementName)).findFirst().orElse(null);
      if (field == null) {
        clazz = clazz.getSuperclass();
      } else {
        break;
      }
    }
    if (field == null) {
      throw new RuntimeException(String
              .format("Для элемента %s отсутствует аннотация Name", elementName));
    }
    return  (SelenideElement) Reflection.extractFieldValue(field, this);
  }

  public static SelenideElement getFieldByName(String fieldName){
    return $(String.format("input[placeholder='%s']", fieldName));
  }



}
