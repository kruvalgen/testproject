package pages;

import annotations.Name;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class SearchResultPage extends BasePage{

  //-----------------------------------------[locators]--------------------------------------------
  @Name("Кнопка Корзина")
  private SelenideElement cartButton = $x("//div[@data-baobab-name='cart'][.//div[text()='Корзина']]");
  //товары из блока популярные предложения
  ElementsCollection productsInPopularOffersBlock = $$x("//div[@data-auto='item']");
  //товары из блока основные предложения
  ElementsCollection productInBasicBlock = $$x("//article[@data-autotest-id='product-snippet']");


  //------------------------------------------[steps]----------------------------------------------
  @Step("Положить товар под номером '{productNumber}' из блока Популярные предложения в корзину")
  public void putProductFromPopularOffersBlockInCart(int productNumber){
    SelenideElement inCartButton = productsInPopularOffersBlock.get(productNumber - 1)
        .$x(".//button[.//span//span[contains(normalize-space(),'корзину')]]")
        .shouldBe(Condition.visible.because("Кнопка не отображается на странице"));
    inCartButton.click();

  }

  @Step("Положить товар под номером '{productNumber}' из основного блока в корзину")
  public void putProductFromBasicBlockInCart(int productNumber){
    SelenideElement inCartButton = productInBasicBlock.get(productNumber - 1)
        .$x(".//button[.//span//span[contains(normalize-space(),'корзину')]]")
        .shouldBe(Condition.visible.because("Кнопка не отображается на странице"));
    inCartButton.click();

  }

  @Step("Сохранить ID товара под номером '{productNumber}' из блока Популярные предложения")
  public String saveProductIdFromPopularOffersBlock(int productNumber){
    SelenideElement product = productsInPopularOffersBlock.get(productNumber - 1);
    String productId = product.$x(".//a").getDomProperty("pathname");
    return productId.substring(productId.lastIndexOf("/"));
  }

  @Step("Сохранить ID товара под номером '{productNumber}' из основного блока")
  public String saveProductIdByNumberFromBasicBlock(int productNumber){
    SelenideElement product = productInBasicBlock.get(productNumber - 1);
    String productId = product.$x(".//h3//a").getDomProperty("pathname");
    return productId.substring(productId.lastIndexOf("/"));
  }


  @Step("В боковом меню, в фильтре '{filterName}' выбрать значение '{value}'")
  public void selectValueFromFilterByName(String filterName, String value){
    SelenideElement filter = getFilterByName(filterName)
        .shouldBe(Condition.visible.because("Фильтр " + filterName + " не найден"));
    filter.$x(String.format(".//label//span[text()='%s']", value))
        .shouldBe(Condition.visible
            .because("Значение " + value + " отсутствует в фильтре " + filterName))
        .click();
  }


//---------------------------------------------[others]---------------------------------------------
  public SelenideElement getFilterByName(String filterName){
    return $x(String.format("//div[@data-zone-name='Filter'][.//h4[text()='%s']]", filterName));
  }

  public SelenideElement getButtonByName(String buttonName){
    return $x(String.format("//div[@data-baobab-name='cart'][.//div[text()='%s']]", buttonName));
  }

}
