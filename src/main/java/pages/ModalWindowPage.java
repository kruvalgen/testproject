package pages;

import annotations.Name;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$x;

public class ModalWindowPage extends BasePage {

  @Name("Модальное окно Товар успешно добавлен в корзину")
  private SelenideElement productAddedInCartModalWindow = $x("//div[@data-auto='upsale-cart-items'][.//h2[text()='Товар успешно добавлен в корзину']]");

  @Name("Модальное окно Удалить выбранные товары?")
  private SelenideElement deletedProductModalWindow = $x("//div[@role='dialog'][.//h2[text()='Удалить выбранные товары?']]");

  @Name("Кнопка 'Удалить' в модальном окне Удалить выбранные данные?")
  private SelenideElement removeButtonInModalWindow = deletedProductModalWindow.$x(".//button[.//span[text()='Удалить']]");
  @Name("Кнопка Продолжить покупки")
  private SelenideElement continueShoppingButton = $x("//button[./span[contains(.,'покупки')]]");

  @Name("Кнопка Крестик(Закрыть)")
  private SelenideElement closeButton = $x("//button[@aria-label='Закрыть']");


  @Step("Кликнуть на кнопку Продолжить покупки в модальном окне '{modalWindowName}'")
  public  void clickOnButtonContinueShoppingFromModalWindow(String modalWindowName){
    SelenideElement modalWindow = getElementByName(modalWindowName)
        .shouldBe(Condition.visible
            .because("Модальное окно с именем " + modalWindowName + " не отображается"));
    SelenideElement button = modalWindow.$x(".//button[./span[contains(.,'покупки')]]")
        .shouldBe(Condition.visible.because("Кнопка Продолжить покупки не отображается"));
    button.click();
  }
}
