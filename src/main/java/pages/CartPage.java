package pages;

import annotations.Name;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class CartPage extends BasePage{

    @Name("Кнопка Удалить всё из корзины")
    private SelenideElement deleteAllButton = $("div[data-zone-name=delete-all]");

    //Товары в корзине
    private ElementsCollection shoppingList = $$x("//div[@data-auto='CartOffer']");

    @Step("Проверить, что ID товара в корзине соответствуют списку {productListIds}")
    public void checkProductInShoppingList(List<String> productListIds){
        List<String> shoppingListName = new ArrayList<>();
        for (SelenideElement element : shoppingList) {
            String link = element.$x(".//span/a").getDomProperty("pathname");
            String id = link.substring(link.lastIndexOf("/"));
            shoppingListName.add(id);
        }
        for (int i = 0; i < shoppingListName.size(); i++) {
            Assertions.assertTrue(productListIds.get(i).equals(shoppingListName.get(i)),
                    "Товар с ID '" + productListIds.get(i) + "' не найден в корзине");
        }
    }

    @Step("Проверить, что корзина пуста")
    public void checkShoppingCartIsEmpty(){
        shoppingList.shouldBe(CollectionCondition.size(0)
                .because("В корзине есть добавленные товары"));
    }
}
