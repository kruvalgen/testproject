package pages;

import annotations.Name;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class MainPage extends BasePage{

  @Name("Кнопка Найти")
  private SelenideElement searchButton = $("button[data-auto='search-button']");


  @Step("Открыть главную страницу")
  public MainPage openMainPage(){
    Selenide.open("https://market.yandex.ru/");
    return this;
  }


}
