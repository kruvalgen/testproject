package tests;


import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Step;
import static com.codeborne.selenide.Selenide.*;

import java.util.Date;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.Cookie;

public class BaseTest {


  @Step("Start the application")
  @BeforeEach
  public void setup() {
  }

  @Step("Stop the application")
  @AfterEach
  public void close() {
    WebDriverRunner.closeWebDriver();
  }




}
