package tests;

import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.Test;
import pages.CartPage;
import pages.MainPage;
import pages.ModalWindowPage;
import pages.SearchResultPage;

import java.util.List;

public class ExampleTest extends BaseTest {

  MainPage mainPage = new MainPage();
  SearchResultPage searchResultPage = new SearchResultPage();
  ModalWindowPage modalWindowPage = new ModalWindowPage();

  CartPage cartPage = new CartPage();

  @Test
  public void test(){

    mainPage.openMainPage();
    mainPage.enterValueInField("Искать товары", "ноутбук");
    mainPage.clickInButton("Кнопка Найти");

    String firstProductName = searchResultPage.saveProductIdFromPopularOffersBlock(1);

    searchResultPage.putProductFromPopularOffersBlockInCart(1);
    modalWindowPage.elementIsVisible("Модальное окно Товар успешно добавлен в корзину");
    modalWindowPage.clickInButton("Кнопка Крестик(Закрыть)");
//    modalWindowPage.clickInButton("Кнопка Продолжить покупки");

    searchResultPage.selectValueFromFilterByName("Производитель", "MSI");
    searchResultPage.selectValueFromFilterByName("Видеокарта", "NVIDIA GeForce RTX 3050");
    String secondProductName = searchResultPage.saveProductIdByNumberFromBasicBlock(3);
    searchResultPage.putProductFromBasicBlockInCart(3);
    modalWindowPage.elementIsVisible("Модальное окно Товар успешно добавлен в корзину");
    modalWindowPage.clickInButton("Кнопка Крестик(Закрыть)");
//    modalWindowPage.clickInButton("Кнопка Продолжить покупки");

    searchResultPage.clickInButton("Кнопка Корзина");

    cartPage.checkProductInShoppingList(List.of(firstProductName, secondProductName));
    cartPage.clickInButton("Кнопка Удалить всё из корзины");

    modalWindowPage.elementIsVisible("Модальное окно Удалить выбранные товары?");
    modalWindowPage.clickInButton("Кнопка 'Удалить' в модальном окне Удалить выбранные данные?");
    cartPage.checkShoppingCartIsEmpty();
  }
}
